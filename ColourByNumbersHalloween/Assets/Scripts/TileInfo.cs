﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TileInfo : MonoBehaviour, IPointerEnterHandler
{
 
    public int expectedNum;

    public int currentNum;

    public Color myColour;

    public ColorManager colorManager;

    public Text textNum;

    public bool correct;


    public void SetUp (){

        currentNum = 69;
        // for (int i = 0; i < colorManager.allTheColours.Count; i++)
        // {
        //     if(colorManager.allTheColours[i] == myColour ){
        //         expectedNum = i;
        //     }

            
        // }

       textNum.text = expectedNum.ToString();

    }


    public void ChangeColor (){

        if(colorManager.done == false){
            GetComponent<Image>().color = colorManager.currentColour;
            currentNum = colorManager.currentNum+1;

            if(expectedNum == currentNum){
                correct = true;
            }
            else{
                correct = false;
            }
        }


    }

    public void OnPointerEnter(PointerEventData eventData)
     {
         if(colorManager.mDown){
             ChangeColor();
         }
     }

   


}
