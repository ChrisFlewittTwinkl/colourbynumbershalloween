﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OptionManager : MonoBehaviour
{
    OptionsData optionsData;
    [Header("Icons")]
    public Sprite muteOnIcon;
    public Sprite muteOffIcon;
    public Sprite expandOnIcon;
    public Sprite expandOffIcon;
    [Header("Buttons")]
    public Image muteBTN; 
    public Image expandBTN;

    void Start()
    {
        optionsData = GameObject.FindGameObjectWithTag("Options data").GetComponent<OptionsData>();
        SetSoundAndScreen();
    }

    void SetSoundAndScreen()
    {
        if(optionsData.muted)
        {
            AudioListener.volume = 0;
            muteBTN.sprite = muteOffIcon;
            
        } else {
            
        }

        if(optionsData.fullscreen)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            expandBTN.sprite = expandOnIcon;
            
        } else {
            
        }
    }

    public void ToggleSound()
    {
        if(optionsData.muted)
        {
            AudioListener.volume = 1;
            muteBTN.sprite = muteOnIcon;
           
            optionsData.muted = false;
        } else {
            AudioListener.volume = 0;
            muteBTN.sprite = muteOffIcon;
            
            optionsData.muted = true;
        }
    }

    public void ToggleScreen()
    {
        if(optionsData.fullscreen)
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
            expandBTN.sprite = expandOnIcon;
           
            optionsData.fullscreen = false;
        } else {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            expandBTN.sprite = expandOffIcon;
            
            optionsData.fullscreen = true;
        }
    }
}